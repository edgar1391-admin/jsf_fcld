/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.managedBeanmodelo;

import com.dto.negocio.IUsuarioDTO;
import com.dto.negocio.imp.IUsuarioDTOImp;
import java.io.Serializable;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

/**
 *
 * @author edgar
 */
@ManagedBean
@SessionScoped
public class ManagedBeanUser implements Serializable {

    
    @ManagedProperty(value = "#{usu}")
    private ManagedBeanUsuarioDepen usu;

    private IUsuarioDTO dto;
    private String mensaje;
    private boolean paso_usuario= false;

    @PostConstruct
    public void inicioConx(){
        dto =new IUsuarioDTOImp();
    }
    
    
    public ManagedBeanUsuarioDepen getUsu() {
        return usu;
    }

    /**
     * @param usu the usu to set
     */
    public void setUsu(ManagedBeanUsuarioDepen usu) {
        this.usu = usu;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String verificarUsuario() {
        if (paso_usuario) {
            return "login";
        } else {
            return "inicio";
        }
    }

    public void loginActionEvent(ActionEvent e) {
        //devuelve el hilo o proceso del request 
        FacesContext facesContext = FacesContext.getCurrentInstance();
        
        //obtengo el comportamiento o los objetos aliados a request
        ExternalContext externalContext = facesContext.getExternalContext();
        
        //indica la petocion esta procesando el evento
//        if (facesContext.isProcessingEvents() == false) {

            if (dto.loginUsuario(getUsu()) != null) {

                //session con el nombre del usuario  
                Map<String, Object> session = externalContext.getSessionMap();
                    paso_usuario = true;             

            } else {
                System.out.println("manager user incorrect");
                setMensaje("usuario incorrecto");
                paso_usuario = false;
               // externalContext.invalidateSession();
            }

//        } else {
//            setUsu(null);
//            setMensaje("hay una petocion en curso");
//            externalContext.invalidateSession();
//        }
    }

}
