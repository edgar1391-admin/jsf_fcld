/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.managedBeanmodelo;

/**
 *
 * @author edgar
 */
public class Country {

   private String nombre; 
   private String code;
   
   public Country(String nombre, String code){
     this.nombre = nombre;
     this.code = code;
   }
   
   
   
   public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    
}
