/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.managedBeanmodelo;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;

/**
 *
 * @author edgar
 */
@ManagedBean(name = "config", eager = true)
@ApplicationScoped
public class ManagedBeanConfiguration {

    private List<Country> countries=new ArrayList<>();

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }
    
    
    public ManagedBeanConfiguration() {
        Country country_1=new Country("Recuplica Domincana","RD");
        Country country_2=new Country("Estados Unidos","EEUU");
        Country country_3=new Country("Japon","JP");
        Country country_4=new Country("Venezuela","VN");
        Country country_5=new Country("Puerto Rico","PR");
        Country country_6=new Country("Mexico","MX");
        Country country_7=new Country("Colombia","CL");
       
        this.countries.add(country_1);
        this.countries.add(country_2);
        this.countries.add(country_3);
        this.countries.add(country_4);
        this.countries.add(country_5);
        this.countries.add(country_6);
        this.countries.add(country_7);
        
    }
    
   
    
    
    
    
   
    
    
    
    
    
    
}
