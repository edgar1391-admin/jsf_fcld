/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.managedBeanmodelo;

import com.bean.RegistroUsuario;
import com.dto.negocio.IUsuarioDTO;
import com.dto.negocio.imp.IUsuarioDTOImp;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;

/**
 *
 * @author edgar.adames
 */
@ManagedBean
@RequestScoped
public class ManagedBeanRegUser {
    
   private RegistroUsuario reg;
   
   //refencia estatica del objeto que registra el usuario
   private final IUsuarioDTO dto = IUsuarioDTOImp.getUsuarioDTOImp();
    
    @ManagedProperty(value = "#{config}")
    private ManagedBeanConfiguration config;
    
    @ManagedProperty(value = "#{managedBeanUser}")
    private ManagedBeanUser managedBeanUsuario;
    
    
    @PostConstruct
    public void init(){
        reg=new RegistroUsuario();
    }

    public RegistroUsuario getReg() {
        return reg;
    }

    public void setReg(RegistroUsuario reg) {
        this.reg = reg;
    }
    
    public String registrarse(){
      if(dto.registroUsuario(getReg()) != null){
            setReg(reg);
            managedBeanUsuario.setMensaje("Registrado con exito! ahora puedes iniciar session");
            return "index";
        }else{
           managedBeanUsuario.setMensaje("No pudo registrase el usuario");
           return "index"; 
        }
    }
    
    public void actionEventRegistrar(ActionEvent e){
        
    }
    
    
    
    
    
  
    
}
