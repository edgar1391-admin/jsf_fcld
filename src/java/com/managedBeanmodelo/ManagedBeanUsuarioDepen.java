/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.managedBeanmodelo;

import com.bean.Usuario;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author edgar.adames
 */
@ManagedBean(name = "usu")
@SessionScoped
public class ManagedBeanUsuarioDepen implements Usuario, Serializable {

 
    public ManagedBeanUsuarioDepen() {
    }
    
    public String nombre; 
    public String password; 
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
}
