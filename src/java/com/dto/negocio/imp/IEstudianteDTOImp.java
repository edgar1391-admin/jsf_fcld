/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dto.negocio.imp;

import com.bean.Estudiante;
import com.dto.negocio.IEstudianteDTO;
import com.dao.persistencia.IEstudianteDao;
import com.dao.persistencia.imp.IEstudianteDaoImp;
import java.util.List;

/**
 *
 * @author edgar.adames
 */
public class IEstudianteDTOImp implements IEstudianteDTO {

    IEstudianteDao persistencia= IEstudianteDaoImp.getIEstudianteDao();
    
    @Override
    public List<Estudiante> listadoEstudiante() {
        List<Estudiante> listaEstudiante = null;
        try{
            listaEstudiante = persistencia.listadoEstudianteDB();
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return listaEstudiante;
    }
    
    
    public static IEstudianteDTO getIEstudianteDTO(){
        return new IEstudianteDTOImp();
    }
    
    
    
    
    
    
    
    
}
