/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dto.negocio.imp;

import com.bean.RegistroUsuario;
import com.bean.Usuario;
import com.dao.persistencia.IUsuarioDao;
import com.dao.persistencia.imp.IUsuarioDaoImp;
import com.dto.negocio.IUsuarioDTO;

/**
 *
 * @author edgar.adames
 */
public class IUsuarioDTOImp implements IUsuarioDTO {

    //clase que tiene la logica de negocio
    
    private final IUsuarioDao persistencia =new IUsuarioDaoImp();

    @Override
    public Usuario loginUsuario(Usuario usuario) {
                    
        if (persistencia.consultarUsuario(usuario) != null) {
             return usuario;
        }else{
             return null;
        }
    
    }

    @Override
    public RegistroUsuario registroUsuario(RegistroUsuario reg) {
      RegistroUsuario regUsu= persistencia.agregarUsuario(reg);
        if(regUsu != null){
              return regUsu;
        }
           return regUsu;
    }
    
    //envia la interfaz en vez de la clase que la implementa
    public static IUsuarioDTO getUsuarioDTOImp(){
        return new IUsuarioDTOImp();
    }
            
    
    
    

}
