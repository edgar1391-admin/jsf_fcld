/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dto.negocio;

import com.bean.Estudiante;
import java.util.List;

/**
 *
 * @author edgar.adames
 */
public interface IEstudianteDTO {
    public List<Estudiante> listadoEstudiante();
}
