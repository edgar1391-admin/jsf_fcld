/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dto.negocio;

/**
 *
 * @author edgar.adames
 */
import com.bean.*;



public interface IUsuarioDTO {
    
    public Usuario loginUsuario(Usuario usuario);
    
    public RegistroUsuario registroUsuario(RegistroUsuario reg);
    
    
}
