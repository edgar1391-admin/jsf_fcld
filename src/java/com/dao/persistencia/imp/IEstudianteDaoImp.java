/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dao.persistencia.imp;

/**
 *
 * @author edgar.adames
 */

import com.bean.Estudiante;
import com.dao.persistencia.IEstudianteDao;
import com.dao.persistencia.db.conx.ConexionBD;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IEstudianteDaoImp implements IEstudianteDao{

    public Connection cox = ConexionBD.getConexion();
    ResultSet rst;
    Statement stm;
    
    public IEstudianteDaoImp() {
        
        try{
           if (cox.isClosed() == false){
               stm = cox.createStatement();
                             
           }else{
               
           }
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
            
    }
    
        
    
    
    
    @Override
    public List<Estudiante> listadoEstudianteDB(){
          List<Estudiante> listaEstudiantes=new ArrayList<>();
        try{
            String query ="SELECT nombre,apellido1,apellido2,fechanacimiento,matricula,activo FROM escuela.estudiante;";
            rst = stm.executeQuery(query);
            
            if(rst.next() != false){

                Estudiante estudiantes=new Estudiante();
                
                estudiantes.setNombre(rst.getString("nombre"));
                estudiantes.setApellido_1(rst.getString("apellido1"));
                estudiantes.setApellido_2(rst.getString("apellido2"));
                estudiantes.setFechanacimiento(new Date(rst.getString("fechanacimiento")));
                estudiantes.setMatricula(rst.getString("matricula"));
                estudiantes.setActivo(Boolean.valueOf(rst.getString("activo")));
                                
                listaEstudiantes.add(estudiantes);
            }
            System.out.println(query);
        }catch(Exception ex){
            ex.printStackTrace();
        }
         return listaEstudiantes;
    }
    
    public static IEstudianteDao getIEstudianteDao(){
        return new IEstudianteDaoImp();
    }
    
    
    
    
}
