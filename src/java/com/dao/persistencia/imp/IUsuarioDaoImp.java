/*  
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dao.persistencia.imp;

import com.bean.RegistroUsuario;
import com.bean.Usuario;
import com.dao.persistencia.IUsuarioDao;
import com.dao.persistencia.db.conx.ConexionBD;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author edgar
 */
public class IUsuarioDaoImp implements IUsuarioDao {

    Statement stm;
    ResultSet rst;
    private final Connection cox = ConexionBD.getConexion();
    private String query;

    @Override
    public Usuario consultarUsuario(Usuario usu) {
        try {
            if (cox.isClosed() == false) {
                query = String.format("SELECT * FROM escuela.func_search_user('%s','%s');", usu.getNombre(), usu.getPassword());
                stm = cox.createStatement();
                rst = stm.executeQuery(query);
                Logger.getLogger("consulta").log(Level.INFO, query);
                //verifica si trae datos
                if (rst.next() != false) {
                    while (rst.next()) {
                        usu.setNombre(rst.getString("nombre"));
                        usu.setPassword(rst.getString("clave"));
                        Logger.getLogger("usuario").log(Level.INFO, ": {0}", usu.getNombre());
                    }
                } else {
                    Logger.getLogger("usuario").log(Level.INFO, ": Usuario {0} incorrecto", usu.getNombre());
                    usu = null;

                }
            } else {
                Logger.getLogger("conexion").log(Level.WARNING, ": La conexion esta cerrada");
                usu = null;
                return usu;
            }
        } catch (SQLException ex) {
            usu = null;
            System.out.println("Excepcion: " + ex.getMessage());
            ex.printStackTrace();
        }
        return usu;
    }

    @Override
    public RegistroUsuario agregarUsuario(RegistroUsuario reg) {

        try {
            if (cox.isClosed() == false) {
                String query = String.format("(SELECT escuela.func_save_estudent(%s,%s,%s,%s,%s,%s,%s,%s,%s)) as save", reg.getNombreUsuario(), reg.getContrasenaUsuario(),
                        reg.getNombre(), reg.getApellido_1(), reg.getApellido_2(), reg.getCedula(), reg.getFechanacimiento().toString(),
                        reg.getLugarNacimiento(), reg.getEmail());

                stm = cox.createStatement();
                rst = stm.executeQuery(query);
                Logger.getLogger("query").log(Level.INFO, ": {0}", query);
                //verifica si trajo dato
                if (rst.next() != false) {
                    if (rst.next()) {
                        boolean registro = rst.getBoolean("save");
                        if (registro) {
                            //aqui debe de retorar el objeto reg activo pasado por parametro 
                            Logger.getLogger("query").log(Level.INFO, ": usuario guardado");
                            return reg;
                        }
                    }
                } else {
                    reg = null;
                    Logger.getLogger("usuario").log(Level.WARNING, ": No se guardo el usuario");
                }
            }else{
                Logger.getLogger("conexion").log(Level.WARNING, ": La conexion esra cerrada");
            }

        } catch (SQLException sqlEx) {
            reg = null;
            Logger.getLogger("error").log(Level.SEVERE, ": Se ha producido un error de consulta");
            Logger.getLogger("error").log(Level.SEVERE, ": {0}", sqlEx.getMessage());
            sqlEx.printStackTrace();
        } catch (Exception ex) {
            reg = null;
            Logger.getLogger("error").log(Level.SEVERE, ": Se ha producido un error en la aplicacion");
            Logger.getLogger("error").log(Level.SEVERE, ": {0}", ex.getMessage());
            ex.printStackTrace();
        }
        return reg;
    }

    @Override
    public Usuario existeUsuario(Usuario usu) {
        try {
            query = String.format("Select nombre from escuela.usuario where nombre='%s'", usu.getNombre());
            if (cox.isClosed() != false) {
                stm = cox.createStatement();
                rst = stm.executeQuery(query);
                Logger.getLogger("query").log(Level.INFO, "{0}", query);

                if (rst.next() == true) {
                    usu.setNombre(rst.getString("nombre"));
                    Logger.getLogger("usuario").log(Level.INFO, ": Ya existe un usuario llamado {0}", usu.getNombre());
                } else {
                    Logger.getLogger("usuario").log(Level.INFO, ": Nombre de usuario {0} valido", usu.getNombre());
                    usu = null;
                }

            } else {
                Logger.getLogger("conexion").log(Level.WARNING, ": La conexion esta cerrada");
            }

        } catch (SQLException sqlEx) {
            Logger.getLogger("error").log(Level.SEVERE, ": Se ha producido un error de consulta");
            Logger.getLogger("error").log(Level.SEVERE, ": {0}", sqlEx.getMessage());
            sqlEx.printStackTrace();
        } catch (Exception ex) {
            Logger.getLogger("error").log(Level.SEVERE, ": Se ha producido un error en la aplicacion");
            Logger.getLogger("error").log(Level.SEVERE, ": {0}", ex.getMessage());
            ex.printStackTrace();
        }
        return usu;
    }

    public static IUsuarioDao getIUsuarioDaoImp() {
        return new IUsuarioDaoImp();
    }

}
