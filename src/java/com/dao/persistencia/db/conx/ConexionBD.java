/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dao.persistencia.db.conx;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
//import org.apache.tomcat.jdbc.pool.PoolUtilities;


public class ConexionBD {
     
   private static Connection cox;
   private final static String usuario="postgres";
   private final static String password="postgres";

  
   
   static{
            try {
            Class.forName("org.postgresql.Driver");
            Logger.getLogger("conexion").log(Level.INFO, ": Driver Cargado");
            cox = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/jsf_fcld",usuario,password);
             //this.cox.setSchema("escuela");
            Logger.getLogger("conexion").log(Level.INFO, ": Conexion JDBC");
            if(ConexionBD.cox == null){
                Logger.getLogger("");
            }
        } catch (SQLException ex) {
            System.out.println("Excepcion: " + ex.getMessage());
            ex.printStackTrace();
        }  catch (ClassNotFoundException ex) {
            System.out.println("Excepcion: " +ex.getMessage());
            ex.printStackTrace();
        }
   }
   

    public static Connection getConexion(){
             return cox;  
    }
    
 
    
    
}
