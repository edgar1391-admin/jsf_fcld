/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dao.persistencia;

/**
 *
 * @author edgar.adames
 */

import com.bean.RegistroUsuario;
import com.bean.Usuario;


public interface IUsuarioDao {
    
     public Usuario consultarUsuario(Usuario usu);
    
     public RegistroUsuario agregarUsuario(RegistroUsuario regUsu);
     
     public Usuario existeUsuario(Usuario usu);
    
}
